# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 22:10:30 2024

@author: yanis
"""
import tkinter as tk
from tkinter import messagebox, simpledialog
import random
import datetime
# Nom du fichier de la banque contenant les informations des comptes
FICHIER_BANQUE = "tableau_banque.txt"

# Fonction pour lire les lignes du fichier de la banque
def lire_fichier():
    """
    Lit les lignes du fichier de la banque.

    Sortie :
    - liste de chaînes de caractères (une par ligne lue dans le fichier)
    """
    with open(FICHIER_BANQUE, 'r') as fichier:
        return fichier.readlines()

# Fonction pour écrire dans le fichier de la banque
def ecrire_dans_fichier(contenu):
    """
    Écrit dans le fichier de la banque.

    Entrée :
    - contenu : chaîne de caractères à écrire dans le fichier
    """
    with open(FICHIER_BANQUE, 'a') as fichier:
        fichier.write(contenu + "\n")

# Fonction pour créer un compte utilisateur
def creer_compte(identifiant, mot_de_passe, solde_livret=0.0):
    """
    Crée un compte utilisateur.

    Entrée :
    - identifiant : identifiant de l'utilisateur à créer (chaîne de caractères)
    - mot_de_passe : mot de passe de l'utilisateur à créer (chaîne de caractères)
    - solde_livret : solde initial du Livret A de l'utilisateur (par défaut à 0.0)
    """
    lignes = lire_fichier()
    for ligne in lignes:
        compte = ligne.strip().split(',')
        if compte[0] == identifiant:
            messagebox.showinfo("Erreur", "L'identifiant est déjà utilisé. Veuillez en choisir un autre.")
            return
    ecrire_dans_fichier(f"{identifiant},{mot_de_passe},50.0,{solde_livret}")  # Ajout de solde_livret dans le fichier
    messagebox.showinfo("Succès", "Compte créé avec succès.")

# Fonction pour connecter un utilisateur
# Fonction pour connecter un utilisateur
def connecter_utilisateur(identifiant, mot_de_passe):
    """
    Connecte un utilisateur.

    Entrée :
    - identifiant : identifiant de l'utilisateur à connecter (chaîne de caractères)
    - mot_de_passe : mot de passe de l'utilisateur à connecter (chaîne de caractères)

    Sortie :
    - dictionnaire représentant les informations de l'utilisateur connecté
      (avec les clés 'identifiant', 'mot_de_passe', 'solde', 'solde_livret')
    """
    lignes = lire_fichier()
    for ligne in lignes:
        compte = ligne.strip().split(',')
        if len(compte) == 4:  # Vérifiez si la ligne comporte bien toutes les informations nécessaires
            if compte[0] == identifiant and compte[1] == mot_de_passe:
                return {'identifiant': identifiant, 'mot_de_passe': mot_de_passe, 'solde': float(compte[2]), 'solde_livret': float(compte[3])}
    messagebox.showinfo("Erreur", "Identifiant ou mot de passe incorrect.")
    return None

# Fonction pour afficher le solde d'un utilisateur connecté
def afficher_solde(utilisateur):
    """
    Affiche le solde d'un utilisateur connecté.

    Entrée :
    - utilisateur : dictionnaire représentant les informations de l'utilisateur connecté
                    (avec les clés 'identifiant', 'mot_de_passe', 'solde')
    """
    if utilisateur:
        messagebox.showinfo("Solde", f"Solde du compte {utilisateur['identifiant']}: {utilisateur['solde']} €")
    else:
        messagebox.showinfo("Erreur", "Veuillez vous connecter pour accéder à cette fonctionnalité.")
# Fonction pour calculer l'intérêt quotidien du Livret A
def calculer_interet_quotidien(solde_livret):
    """
    Calcule l'intérêt quotidien du Livret A.

    Entrée :
    - solde_livret : solde actuel du Livret A (float)

    Sortie :
    - intérêt quotidien du Livret A (float)
    """
    taux_interet_annuel = 0.04  # Taux d'intérêt annuel du Livret A (4%)
    interet_annuel = solde_livret * taux_interet_annuel
    interet_quotidien = interet_annuel / 365  # Nombre de jours dans une année
    return interet_quotidien

# Fonction pour mettre à jour le solde du Livret A dans le fichier de la banque
def mettre_a_jour_livret_a(utilisateur):
    """
    Met à jour le solde du Livret A dans le fichier de la banque en fonction de l'intérêt quotidien.

    Entrée :
    - utilisateur : dictionnaire représentant les informations de l'utilisateur connecté
                    (avec les clés 'identifiant', 'mot_de_passe', 'solde', 'solde_livret')
    """
    if utilisateur:
        date_derniere_mise_a_jour = datetime.datetime.strptime(utilisateur['date_maj_livret'], '%Y-%m-%d').date() if 'date_maj_livret' in utilisateur else datetime.date.today()
        date_actuelle = datetime.date.today()
        nombre_jours_ecoules = (date_actuelle - date_derniere_mise_a_jour).days
        interet_quotidien = calculer_interet_quotidien(utilisateur['solde_livret'])
        nouveau_solde_livret = utilisateur['solde_livret'] + interet_quotidien * nombre_jours_ecoules

        lignes = lire_fichier()
        with open(FICHIER_BANQUE, 'w') as fichier:
            for ligne in lignes:
                compte = ligne.strip().split(',')
                if compte[0] == utilisateur['identifiant']:
                    fichier.write(f"{compte[0]},{compte[1]},{utilisateur['solde']},{nouveau_solde_livret}\n")
                else:
                    fichier.write(ligne)

        utilisateur['solde_livret'] = nouveau_solde_livret
        utilisateur['date_maj_livret'] = date_actuelle.strftime('%Y-%m-%d')


# Fonction pour simuler un investissement dans la bourse
def investir_bourse(utilisateur):
    """
    Simule un investissement dans la bourse pour un utilisateur connecté.

    Entrée :
    - utilisateur : dictionnaire représentant les informations de l'utilisateur connecté
                    (avec les clés 'identifiant', 'mot_de_passe', 'solde')
    """
    if utilisateur:
        options_bourse = {
            'Google': 0.10,
            'Apple': 0.15,
            'Amazon': 0.20,
            'Tesla': 0.25,
            'Microsoft': 0.12
        }
        option = simpledialog.askstring("Investir dans la bourse", "Choisissez une option d'investissement (Google, Apple, Amazon, Tesla, Microsoft): ")
        if option in options_bourse:
            taux_hausse = options_bourse[option]
            montant = simpledialog.askfloat("Investir dans la bourse", "Entrez le montant de l'investissement: ")
            gain_ou_perte = random.choices([-1, 1], weights=[0.4, 0.6])[0]  # Gain: 60%, Perte: 40%
            montant_final = montant * (1 + taux_hausse * gain_ou_perte)
            utilisateur['solde'] += montant_final
            lignes = lire_fichier()
            with open(FICHIER_BANQUE, 'w') as fichier:
                for ligne in lignes:
                    compte = ligne.strip().split(',')
                    if compte[0] == utilisateur['identifiant']:
                        fichier.write(f"{compte[0]},{compte[1]},{utilisateur['solde']}\n")
                    else:
                        fichier.write(ligne)
            if gain_ou_perte == 1:
                messagebox.showinfo("Investissement", f"Vous avez gagné {montant_final - montant} € en investissant dans {option}.")
            else:
                messagebox.showinfo("Investissement", f"Vous avez perdu {montant - montant_final} € en investissant dans {option}.")
            messagebox.showinfo("Solde", f"Votre solde actuel est de {utilisateur['solde']} €.")
        else:
            messagebox.showinfo("Erreur", "Option invalide. Veuillez choisir une option valide.")
    else:
        messagebox.showinfo("Erreur", "Veuillez vous connecter pour investir dans la bourse.")

def ouvrir_livret_a(utilisateur):
    """
    Ouvre un Livret A pour un utilisateur connecté.

    Entrée :
    - utilisateur : dictionnaire représentant les informations de l'utilisateur connecté
                    (avec les clés 'identifiant', 'mot_de_passe', 'solde', 'solde_livret')
    """
    if utilisateur:
        montant_initial = simpledialog.askfloat("Ouvrir un Livret A", "Entrez le montant initial à déposer dans votre Livret A: ")
        if montant_initial is not None and montant_initial < utilisateur['solde']:
            utilisateur['solde_livret'] += montant_initial  # Modifier ici la clé pour 'solde_livret'
            lignes = lire_fichier()
            with open(FICHIER_BANQUE, 'w') as fichier:
                for ligne in lignes:
                    compte = ligne.strip().split(',')
                    if compte[0] == utilisateur['identifiant']:
                        fichier.write(f"{compte[0]},{compte[1]},{utilisateur['solde']},{utilisateur['solde_livret']}\n")  # Mettre à jour également ici
                    else:
                        fichier.write(ligne)
            messagebox.showinfo("Livret A", f"Vous avez ouvert un Livret A avec un montant initial de {montant_initial} €.")
            messagebox.showinfo("Solde", f"Votre solde actuel est de {utilisateur['solde_livret']} €.")
    else:
        messagebox.showinfo("Erreur", "Veuillez vous connecter pour ouvrir un Livret A.")

# Fonction pour afficher l'aide
def afficher_aide():
    """
    Affiche l'aide avec des questions et leurs réponses sur l'interface.
    """
    aide = {
        "Comment créer un compte ?": "Cliquez sur le bouton 'Créer un compte' et suivez les instructions.",
        "Comment se connecter ?": "Cliquez sur le bouton 'Se connecter' et saisissez vos identifiants.",
        "Comment afficher le solde ?": "Une fois connecté, cliquez sur le bouton 'Afficher le solde'.",
        "Comment investir dans la bourse ?": "Une fois connecté, cliquez sur le bouton 'Investir dans la bourse' et suivez les instructions.",
        "Comment ouvrir un Livret A ?": "Une fois connecté, cliquez sur le bouton 'Ouvrir un Livret A' et suivez les instructions."
    }
    message = "\n".join([f"{question}\n{reponse}\n" for question, reponse in aide.items()])
    messagebox.showinfo("Aide", message)

# Fonction pour créer une fenêtre de création de compte
def creer_compte_window():
    """
    Crée une fenêtre de création de compte.
    """
    def creer():
        identifiant = identifiant_entry.get()
        mot_de_passe = mot_de_passe_entry.get()
        creer_compte(identifiant, mot_de_passe)
        creer_compte_window.destroy()

    creer_compte_window = tk.Toplevel(root)
    creer_compte_window.title("Créer un compte")
    creer_compte_window.config(bg='white')

    identifiant_label = tk.Label(creer_compte_window, text="Identifiant:", bg='white', fg='black', font=('Helvetica', 14))
    identifiant_label.grid(row=0, column=0, padx=10, pady=5)
    identifiant_entry = tk.Entry(creer_compte_window, font=('Helvetica', 14))
    identifiant_entry.grid(row=0, column=1, padx=10, pady=5)

    mot_de_passe_label = tk.Label(creer_compte_window, text="Mot de passe:", bg='white', fg='black', font=('Helvetica', 14))
    mot_de_passe_label.grid(row=1, column=0, padx=10, pady=5)
    mot_de_passe_entry = tk.Entry(creer_compte_window, show="*", font=('Helvetica', 14))
    mot_de_passe_entry.grid(row=1, column=1, padx=10, pady=5)

    creer_button = tk.Button(creer_compte_window, text="Créer", command=creer, font=('Helvetica', 14), bd=4, relief="raised")
    creer_button.grid(row=2, columnspan=2, padx=10, pady=10)

# Fonction pour créer une fenêtre de connexion
def se_connecter_window():
    """
    Crée une fenêtre de connexion.
    """
    identifiant = simpledialog.askstring("Se connecter", "Entrez votre identifiant: ")
    mot_de_passe = simpledialog.askstring("Se connecter", "Entrez votre mot de passe: ")
    utilisateur = connecter_utilisateur(identifiant, mot_de_passe)
    if utilisateur:
        messagebox.showinfo("Connexion réussie", f"Connexion réussie en tant que {identifiant}.")
        # Appel pour mettre à jour le solde du Livret A
        mettre_a_jour_livret_a(utilisateur)
        gestion_compte_window(utilisateur)
    else:
        messagebox.showinfo("Erreur", "Identifiant ou mot de passe incorrect.")

# Fonction pour créer une fenêtre de gestion de compte
def gestion_compte_window(utilisateur):
    """
    Crée une fenêtre de gestion de compte.

    Entrée :
    - utilisateur : dictionnaire représentant les informations de l'utilisateur connecté
                    (avec les clés 'identifiant', 'mot_de_passe', 'solde')
    """
    root = tk.Toplevel()
    root.title("Gestionnaire de Compte")
    root.config(bg='white')

    afficher_solde_button = tk.Button(root, text="Afficher le solde", command=lambda: afficher_solde(utilisateur), font=('Helvetica', 14), bd=4, relief="raised")
    afficher_solde_button.pack(pady=10)

    investir_bourse_button = tk.Button(root, text="Investir dans la bourse", command=lambda: investir_bourse(utilisateur), font=('Helvetica', 14), bd=4, relief="raised")
    investir_bourse_button.pack(pady=10)

    ouvrir_livret_a_button = tk.Button(root, text="Ouvrir un Livret A", command=lambda: ouvrir_livret_a(utilisateur), font=('Helvetica', 14), bd=4, relief="raised")
    ouvrir_livret_a_button.pack(pady=10)

# Création de la fenêtre principale
root = tk.Tk()
root.title("Bank'onnect")
root.geometry("600x400")
root.config(bg='black')
root.iconbitmap("3.ico")  # Ajout de l'icône

# Création du cadre pour organiser les widgets
frame = tk.Frame(root, bg='black')
frame.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

# Titre
title_label = tk.Label(frame, text="Bank'onnect", font=('Helvetica', 24, 'bold'), bg='black', fg='white')
title_label.pack(pady=20)

# Boutons
creer_compte_button = tk.Button(frame, text="Créer un compte", command=creer_compte_window, font=('Helvetica', 14), bg='white', fg='black', bd=4, relief="raised")
creer_compte_button.pack(pady=10)

se_connecter_button = tk.Button(frame, text="Se connecter", command=se_connecter_window, font=('Helvetica', 14), bg='white', fg='black', bd=4, relief="raised")
se_connecter_button.pack(pady=10)

aide_button = tk.Button(frame, text="Aide", command=afficher_aide, font=('Helvetica', 14), bg='white', fg='black', bd=4, relief="raised")
aide_button.pack(pady=10)

quitter_button = tk.Button(frame, text="Quitter", command=root.destroy, font=('Helvetica', 14), bg='white', fg='black', bd=4, relief="raised")
quitter_button.pack(pady=10)

root.mainloop()

